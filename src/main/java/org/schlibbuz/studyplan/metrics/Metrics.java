/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan.metrics;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Responsible for measuring code performance.
 * @author stefan
 */
public class Metrics {

  private static long measurementDuration;
  private final DecimalFormat df;

  private final Map<Long, String> pendingMeasurements;

  /**
   * Instantiates a metrics object.
   */
  public Metrics() {
    this.pendingMeasurements = new HashMap<>();
    df = new DecimalFormat("#.###ms");
    calibrate();
  }

  /**
   * Measures how long the measuring-process takes.
   */
  public final void calibrate() {
    measurementDuration = 0;
    long peek = System.nanoTime();
    peekStart(peek, "calibrate");
    measurementDuration = peekStop(peek);
  }

  /**
   * Starts measuring a method.
   * @param peek the nanotime when the peek was started
   * @param methodName the name of the peeked method
   */
  public void peekStart(long peek, String methodName) {
    pendingMeasurements.put(peek, methodName);
    System.nanoTime();
  }

  /**
   * Ends the measuring for a particular method.
   * @param peek hashkey
   * @return long
   */
  public long peekStop(long peek) {
    var methodName = pendingMeasurements.remove(peek);
    long duration = System.nanoTime() - peek - measurementDuration;
    System.out.println(methodName + " took " + df.format(duration / 1000000f));
    return duration;
  }

  /**
   * The duration is subtracted when measuring a sequence.
   * @return long
   */
  public static long getMeasurementDuration() {
    return measurementDuration;
  }

}
