/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan.cache;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.schlibbuz.studyplan.metrics.Metrics;

/**
 * Holds cached catalogue data.
 * @author stefan
 */
public class ModuleCache {

  private static final Map<String, Map<String, Set<String>>> MODULE_CACHE = new HashMap<>();
  private static final Metrics metrics = new Metrics();

  /**
   * Build a modules map form given data-file.
   * @throws IOException in case of i/o error
   */
  public static Map<String, Set<String>> buildModuleCacheFromFile(final String name) throws IOException {
    long peek = System.nanoTime();
    metrics.peekStart(peek, "buildModuleCacheFromFile");
    Map<String, Set<String>> cache = new HashMap<>();
    FileUtils.readLines(new File(new StringBuilder("src/main/resources/").append(name).toString()), "UTF-8")
        .stream().map(line -> line.trim())
        .filter(line -> !line.isBlank())
        .forEach(moduleString -> {
          String[] moduleParts = moduleString.split(" ");
          String moduleName = null;
          Set<String> moduleDeps = new HashSet<>();
          for (int i = 0; i < moduleParts.length; i += 1) {
            if (i == 0) {
              moduleName = moduleParts[i];
            } else {
              moduleDeps.add(moduleParts[i]);
            }
          }
          cache.put(moduleName, moduleDeps);
        });
    metrics.peekStop(peek);
    return cache;
  }

  /**
   * Gets the modulecache for a given type of studyplan. If the cache is not
   *     present, it is built.
   * @param name the name of the cache you want to pull
   * @return Map
   * @throws IOException in case of i/o error
   */
  public static Map<String, Set<String>> getCache(final String name) throws IOException {
    if (!MODULE_CACHE.containsKey(name)) {
      MODULE_CACHE.put(
          name,
          buildModuleCacheFromFile(name)
      );
    }
    return MODULE_CACHE.get(name);
  }

}
