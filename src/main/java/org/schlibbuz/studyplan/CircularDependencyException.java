/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan;

/**
 * Throws when a circular dependency in the studyplan is discovered.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class CircularDependencyException extends Exception {

  /**
   * Creates a new instance of <code>CircularDependencyException</code> without detail message.
   */
  public CircularDependencyException() {
  }

  /**
   * Constructs an instance of <code>CircularDependencyException</code> with the specified detail message.
   *
   * @param msg the detail message.
   */
  public CircularDependencyException(String msg) {
    super(msg);
  }
}
