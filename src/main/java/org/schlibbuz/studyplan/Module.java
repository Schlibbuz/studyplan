/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan;

import java.util.Objects;
import java.util.Set;

/**
 * Represents a module from the studyplan.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
final class Module implements Comparable<Module> {


  private final String name;
  private final Set<String> deps;

  /**
   * Represents a module.
   * @param name the name of the module
   * @param deps the dependencies it has
   */
  public Module(final String name, final Set<String> deps) {
    this.name = name;
    this.deps = deps;
  }

  /**
   * Getter for name.
   * @return String
   */
  public String getName() {
    return name;
  }

  /**
   * Getter for deps.
   * @return List
   */
  public Set<String> getDeps() {
    return deps;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 71 * hash + Objects.hashCode(this.name);
    hash = 71 * hash + Objects.hashCode(this.deps);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Module other = (Module) obj;
    if (!Objects.equals(this.name, other.name)) {
      return false;
    }
    return Objects.equals(this.deps, other.deps);
  }

  @Override
  public String toString() {
    return "Module{" + "name=" + name + ", deps=" + deps + '}';
  }

  @Override
  public int compareTo(Module t) {
    return getName().compareTo(t.getName());
  }

}
