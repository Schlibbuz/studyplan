/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.schlibbuz.studyplan.cache.ModuleCache;
import org.schlibbuz.studyplan.metrics.Metrics;

/**
 * Class StudyPlan.
 * @author stefan
 */
public class StudyPlan {

  private static final Metrics metrics = new Metrics();

  private final Map<String, Module> modules = new HashMap<>();
  private final String name;

  /**
   * Instance with given name.
   * @param name the type of plan to calculate
   * @throws java.io.IOException in case of i/o error
   */
  public StudyPlan(String name) throws IOException {
    this.name = name;
    buildModules();
  }


  /**
   * Main method of class StudyPlan.
   * @param args the command line arguments
   * @throws java.io.IOException in case of i/o error
   * @throws org.schlibbuz.studyplan.CircularDependencyException in case a circular dependency is detected.
   */
  public static void main(String[] args) throws IOException, CircularDependencyException {
    var studyPlanName = "studyCatalogueNormal";
    if (args.length > 1) {
      studyPlanName = args[1];
    }
    var studyPlan = new StudyPlan(studyPlanName);
    studyPlan.print();
  }

  /**
   * Prints the studyplan.
   */
  void print() throws CircularDependencyException {
    long peek = System.nanoTime();
    metrics.peekStart(peek, "printStudyplan");
    int semesterNumber = 0;
    while (!modules.isEmpty()) {
      semesterNumber += 1;
      var semesterModules = modules.values()
          .stream()
          .filter(module -> module.getDeps().isEmpty()).collect(Collectors.toSet());
      if (semesterModules.isEmpty()) {
        throw new CircularDependencyException("circular dependency detected, cannot proceed!");
      }
      System.out.println("Semester " + semesterNumber + ":");
      semesterModules.forEach(module -> {
        var moduleName = module.getName();
        System.out.println(moduleName);
        removeModule(moduleName);
      });
      System.out.println("");
    }
    metrics.peekStop(peek);
  }

  /**
   * Print current moduleMap state.
   */
  void printModules() {
    modules.values().forEach(module -> {
      System.out.println(module);
    });
  }

  /**
   * Convenience method. Calls removeModule with deps set to true.
   * @param moduleName the name of the module to remove
   */
  void removeModule(String moduleName) {
    removeModule(moduleName, true);
  }

  /**
   * Remove a module.
   * @param moduleName the name of the module to remove
   * @param includeDependencies whether we want to remove dependencies too
   */
  void removeModule(final String moduleName, final boolean includeDependencies) {
    if (includeDependencies) {
      Map<String, Module> affected = modules.entrySet()
          .stream()
          .filter(kv -> kv.getValue().getDeps().contains(moduleName))
          .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
      affected.keySet().forEach(k -> {
        modules.get(k).getDeps().remove(moduleName);
      });
    }
    modules.remove(moduleName);
  }

  /**
   * Builds modules form module-cache.
   */
  final void buildModules() throws IOException {
    long peek = System.nanoTime();
    metrics.peekStart(peek, "buildModules");
    ModuleCache.getCache(name).forEach((moduleName, moduleDependencies) -> {
      buildModule(moduleName, moduleDependencies);
    });
    metrics.peekStop(peek);
  }

  /**
   * Build a module.
   * @param moduleName the name of the module to build
   * @param moduleDeps the dependencies the module has
   */
  void buildModule(String moduleName, Set<String> moduleDependecies) {
    var module = new Module(moduleName, new HashSet<>(moduleDependecies));
    modules.put(moduleName, module);
  }

}
