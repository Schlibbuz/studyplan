/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan.metrics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

/**
 * Tests class Metrics.
 *
 * @author stefan
 */
public class MetricsTest {

  /**
   * Runs before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Tests calibration method, of class Metrics.
   */
  @RepeatedTest(10)
  public void testCalibrate() {
    var metrics = new Metrics();
  }

}
