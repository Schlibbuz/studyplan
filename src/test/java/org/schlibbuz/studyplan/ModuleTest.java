/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.io.IOException;
import java.util.HashSet;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test for class Module.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class ModuleTest {

  private Module module;

  /**
   * Constructor.
   */
  public ModuleTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   * @throws java.io.IOException in case of i/o error
   */
  @BeforeEach
  public void setUp() throws IOException {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Tests constructor of class Module.
   */
  @Test
  void testConstruct() {

  }

  /**
   * Tests constructor of class Module.
   */
  @Test
  void testConstruct_Except() {

  }

  /**
   * Test of equals method, of class Module.
   */
  @Test
  public void testEqualsContract() {
    EqualsVerifier.forClass(Module.class).verify();
  }

  /**
   * Test of compareTo method, of class Module.
   */
  @Test
  void testCompareTo() {
    var m1 = new Module("aaa", new HashSet<>());
    var m2 = new Module("aab", new HashSet<>());
    assertAll("compareTo",
        () -> assertThat("lexical normal", m1.compareTo(m2), lessThan(0)),
        () -> assertThat("lexical reverse", m2.compareTo(m1), greaterThan(0)),
        () -> assertThat("lexical equal", m1.compareTo(m1), equalTo(0))
    );

  }
}
