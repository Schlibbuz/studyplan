/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test for class StudyPlan.
 * @author stefan
 */
public class StudyPlanTest {

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of main method, of class StudyPlan.
   *
   * @throws java.io.IOException in case of i/o error
   * @throws org.schlibbuz.studyplan.CircularDependencyException if a circular dependency is detected
   */
  @Test
  public void testMain() throws IOException, CircularDependencyException {
    System.out.println("main");
    StudyPlan.main(new String[]{"StudyPlan.java","studyCatalogueNormal"});
  }

  /**
   * Test of print method, of class StudyPlan.
   *
   * @throws org.schlibbuz.studyplan.CircularDependencyException if a circular dependency is detected
   */
  @Test
  public void testPrintStudyplan() throws CircularDependencyException, IOException {
    System.out.println("print");
    var studyPlan = new StudyPlan("studyCatalogueNormal");
    studyPlan.print();
  }

  /**
   * Test removeModule method, of class StudyPlan.
   */
  @Test
  void testRemoveModule() throws IOException {
    var studyPlan = new StudyPlan("studyCatalogueTest");
    System.out.println("#########################################");
    studyPlan.printModules();
    studyPlan.removeModule("OO");
    System.out.println("#########################################");
    studyPlan.printModules();
  }

}
