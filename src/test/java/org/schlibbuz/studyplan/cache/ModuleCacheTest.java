/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.studyplan.cache;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests class ModuleCache.
 * @author stefan
 */
public class ModuleCacheTest {

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of buildModuleCacheFromFile method, of class ModuleCache.
   */
  @Test
  public void testBuildModuleCacheFromFile() throws Exception {
    System.out.println("buildModuleCacheFromFile");
    String name = "studyCatalogueTest";
    Map<String, Set<String>> result = ModuleCache.buildModuleCacheFromFile(name);
    assertAll("getCache",
        () -> assertEquals(3, result.size(), "Map-Size"),
        () -> assertEquals(
            new HashSet<>(Arrays.asList("OO", "DB1", "DB2")),
            result.keySet(),
            "Map-Keys"
        ),
        () -> assertEquals(
            new HashSet<Set>(Arrays.asList(
                new HashSet<>(),
                new HashSet<>(Arrays.asList("OO")),
                new HashSet<>(Arrays.asList("DB1"))
            )),
            result.values().stream().collect(Collectors.toSet()),
            "Map-Values"
        )
    );
  }

  /**
   * Test of buildModuleCacheFromFile method, of class ModuleCache.
   */
  @Test
  public void testBuildModuleCacheFromFile_Fail() throws Exception {
    System.out.println("buildModuleCacheFromFile");
    String name = "studyCatalogueFail";
    Map<String, Set<String>> result = ModuleCache.buildModuleCacheFromFile(name);
    assertAll("getCache",
        () -> assertEquals(3, result.size(), "Map-Size"),
        () -> assertEquals(
            new HashSet<>(Arrays.asList("OO", "DB1", "DB2")),
            result.keySet(),
            "Map-Keys"
        ),
        () -> assertEquals(
            new HashSet<Set>(Arrays.asList(
                new HashSet<>(),
                new HashSet<>(Arrays.asList("OO", "DB2")),
                new HashSet<>(Arrays.asList("DB1"))
            )),
            result.values().stream().collect(Collectors.toSet()),
            "Map-Values"
        )
    );
  }

  /**
   * Test of getCache method, of class ModuleCache.
   */
  @Test
  public void testGetCache() throws Exception {
    System.out.println("getCache");
    String name = "studyCatalogueTest";
    Map<String, Set<String>> result = ModuleCache.getCache(name);
  }

}
